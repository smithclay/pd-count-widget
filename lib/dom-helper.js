exports.getInsertionPoint = function() {
	var scripts = document.getElementsByTagName('script');
	for (var i = 0; i < scripts.length; i++) {
		var s = scripts[i];
		if (s.getAttribute('class') === 'pd-count-widget' && 
			s.getAttribute('data-processed') !== 'y') {
			s.setAttribute('data-processed', 'y')
			return s;
		}
	}
};

exports.getConfig = function(element) {
	if (!element) {
		return {};
	}

	return {
		host: element.getAttribute('data-host'),
		status: element.getAttribute('data-status'),
		service: element.getAttribute('data-service'),
		token: element.getAttribute('data-token')
	};
};

var WIDTH = '460';
var HEIGHT = '225';

var colors = {
	"open": "#005c9c",
	"triggered": "#a60000",
	"acknowledged": "#ff4500",
	"resolved": "#00a600"
};

var stylesheets = [".count { font-size: 7em; font-weight: bold; font-family: 'Lato', sans-serif; }",
	'div { min-height: ' + HEIGHT + 'px; min-width: ' + WIDTH + 'px; text-align: center; border-radius: 10px; }',
	".status { display: block; font-size: 2.25em; font-family: 'Lato', sans-serif; }"];

exports.createBadge = function(frameDocument, count, config) {
	var font = frameDocument.createElement('link');
	font.setAttribute('href', 'http://fonts.googleapis.com/css?family=Lato');
	font.setAttribute('rel', 'stylesheet');
	font.setAttribute('type', 'text/css');
	frameDocument.body.insertBefore(font, null);

	var style = frameDocument.createElement('style');
	style.appendChild(frameDocument.createTextNode(stylesheets.join(' ')));
	frameDocument.body.insertBefore(style, null);

	var widgetDiv = frameDocument.createElement('div');
	widgetDiv.style.backgroundColor = colors[config.status];
	widgetDiv.innerHTML = '<span class="count">' +  count +
		'</span><span class="status">' + config.status + ' incidents</span>';
	frameDocument.body.insertBefore(widgetDiv, null);
};

exports.createFrame = function() {
	var iframe = document.createElement('iframe');
	iframe.setAttribute('class', 'pd-count-widget');
	iframe.setAttribute('frameborder', 0);
	iframe.setAttribute('scrolling', 'no');
	iframe.setAttribute('width', WIDTH);
	iframe.setAttribute('height', HEIGHT);	
	iframe.setAttribute('title', 'PagerDuty Counts Widget');
	iframe.setAttribute('style', 'border: none; max-width: 100%; min-width: 180px');
	return iframe;
};