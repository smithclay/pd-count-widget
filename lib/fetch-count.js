var reqwest = require('reqwest');

exports.fetchCount = function(options, cb) {
	reqwest({
	  url: 'http://' + options.host + '/api/v1/incidents/count?status=' 
	  	+ options.status + '&service=' + (options.service || ''),
	  method: 'get',
	  type: 'json',
	  headers: {
	    'Authorization': 'Token token=' + options.token
	  },
	  error: function(error) {
	  	cb(error, null);
	  },
	  success: function(data) {
	    var total = data.total;
	    cb(null, total);
	  },
	  crossOrigin: true
	});
};