var domHelper = require('./lib/dom-helper');
var fetchCount = require('./lib/fetch-count').fetchCount;

var script = domHelper.getInsertionPoint();
var frame = domHelper.createFrame();

script.parentElement.insertBefore(frame, script);


var config = domHelper.getConfig(script);
fetchCount(config, function(err, count) {
	if (err) {
		return;
	}

	domHelper.createBadge(frame.contentWindow.document, count, config);

});