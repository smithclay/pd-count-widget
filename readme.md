# PagerDuty Incidents Count Widget Example

A widget that displays PagerDuty incident counts on a webpage.

![Alt text](example/example-screenshot.png)

## Usage

Configure the following `script` tag (or multiple script tags for multiple widgets) somewhere on an *internal* page (such as a wiki).

* Do not use this widget on the open internet, as your API key will be in the source.*

Possible values for `data-status` are `open`, `triggered`, or `resolved`. See the [PagerDuty Incidents Count API](http://developer.pagerduty.com/documentation/rest/incidents/count) for more information.

```
<script class="pd-count-widget" data-host="<your-subdomain>.pagerduty.com" data-status="open" data-token="YOUR-PD-API-TOKEN" src="pd-count-widget.js"></script>
```

## Dependencies

* node > `0.8`

## Building

``sh
sh $ npm install
sh $ browserify index.js > pd-count-widget.js
``

## License

BSD